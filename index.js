
const HTTP=require('http');
const PORT=4000;

HTTP.createServer((req,res)=>{

    if(req.url== "/profile" && req.method=="GET"){
        res.writeHead(200, {"Content-Type": "plain/text"});
        res.end("Welcome to your profile")     
    }else if(req.url=="/courses" && req.method=="GET"){
        res.writeHead(200,{"Content-Type": "plain/text"});
        res.end("Here's out courses available")
    }else if(req.url=="/" && req.method=="GET"){
        res.writeHead(200,{"Content-Type": "plain/text"});
        res.end("Welcome")
    }else if(req.url=="/addCourses" && req.method=="POST"){
        res.writeHead(200,{"Content-Type": "plain/text"});
        res.end("Add course to our resource")
    }else if(req.url=="/updateCourses" && req.method=="PUT"){
        res.writeHead(200,{"Content-Type": "plain/text"});
        res.end("Update a course to our resource")
    }else if(req.url=="/archiveCourses" && req.method=="DELETE"){
        res.writeHead(200,{"Content-Type": "plain/text"});
        res.end("Archve courses to our resource")
    }

}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));